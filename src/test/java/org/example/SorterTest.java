package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SorterTest {

    private List<Integer> input;
    private List<Integer> expected;

    public SorterTest(List<Integer> input, List<Integer> expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { Arrays.asList(5, 4, 3, 2, 1), Arrays.asList(1, 2, 3, 4, 5) },
                { Arrays.asList(1, 2, 3, 4, 5), Arrays.asList(1, 2, 3, 4, 5) },
                // Add more lists here
        });
    }

    @Test
    public void testSortList() {
        // Arrange
        Sorter sorter = new Sorter();

        // Act
        List<Integer> result = sorter.sortList(input);

        // Assert
        Assert.assertEquals(expected, result);
    }
}
