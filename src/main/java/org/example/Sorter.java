package org.example;



import java.util.Collections;
import java.util.List;
public class Sorter {

    public List<Integer> sortList(List<Integer> numbers) {
        Collections.sort(numbers);
        return numbers;
    }
}
